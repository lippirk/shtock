let
  haskellPackages =
    if compiler == "default"
    then pkgs.haskellPackages
    else pkgs.haskell.packages.${compiler};

  shtock = haskellPackages.callCabal2nix "shtock" ./server {};

  # pinned nixpkgs
  pkgs = import (builtins.fetchTarball {
    name = "nixos-18.09-11-02-19";
    # `git ls-remote https://github.com/nixos/nixpkgs BRANCH_NAME`
    url = https://github.com/nixos/nixpkgs/archive/19a0543c62847c6677c2563fc8c986c1c82f2ea3.tar.gz;
    # Hash obtained using `nix-prefetch-url --unpack <url>`
    sha256 = "13ndciddbqi5j6b5pajyx476aq5idpk4jsjaiw76y7ygnqj3y239";
  }) {};

  compiler = "ghc844";
in
  {
    pkgs = pkgs;
    shtock = shtock;
    haskellPackages = haskellPackages;
  }
