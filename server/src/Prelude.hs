{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists  #-}

module Prelude
  ( module P
  , module ReaderMonad
  -- lists
  , Vector
  , safeHead
  -- Text
  , String -- use base Data.String instead of Foundation String
  , Text
  , ByteString
  , LByteString
  , unpack
  , print
  , show
  , tshow
  , readMaybe
  -- tuple
  , map4Tuple
  , map5Tuple
  -- Monad
  , (<=<)
  , mapM
  -- MonadIO
  , liftIO
  , MonadIO
  -- class
  , lift
  , Alternative
  -- exceptions
  , MonadCatch
  , MonadThrow
  , try
  , catch
  , throwM
  -- GHC
  , Generic
  -- misc
  , toEnumIfInBounds
  , maybeToEither
  , comparing
  , (&)
  -- aeson
  , noUnderscoreAndOmitNothingFields
  -- endo
  , Endo(..)
  , succForever
  -- debug
  , debugWrtLn
  , trace
  -- gdp
  , the
  ) where

import           Control.Monad.IO.Class     (MonadIO, liftIO)
import           Control.Monad.Trans.Reader as ReaderMonad
import qualified Data.List                  as L
import           Data.Text                  (Text, unpack)
import qualified Data.Text                  as T
import           Data.Vector                (Vector)
import           Foundation                 as P hiding (String, error, show)

import           Control.Applicative        (Alternative)
import           Control.Monad              (mapM, (<=<))
import           Control.Monad.Catch        (MonadCatch, MonadThrow, catch,
                                             throwM, try)
import           Control.Monad.Trans.Class  (lift)
import qualified Data.Aeson                 as A
import           Data.ByteString            (ByteString)
import qualified Data.ByteString.Lazy       as LBS
import           Data.Function              ((&))
import           Data.Monoid                (Endo (..))
import           Data.Ord                   (comparing)
import           Data.String                (String)
import           Debug.Trace                (trace)
import           GDP                        (the)
import           GHC.Generics               (Generic)
import           System.IO                  (print)
import qualified Text.Read                  as TR
import           Text.Show                  (show)

type LByteString = LBS.ByteString

maybeToEither :: e -> Maybe a -> Either e a
maybeToEither e mx =
  case mx of
    Nothing -> Left e
    Just x  -> Right x

readMaybe :: TR.Read a => Text -> Maybe a
readMaybe = TR.readMaybe . unpack

safeHead :: [a] -> Maybe a
safeHead xs =
  if L.null xs
    then Nothing
    else Just $ L.head xs

tshow :: Show a => a -> Text
tshow = T.pack . show

-- | Useful for converting integers to their enum counterparts
toEnumIfInBounds :: (Enum t, Bounded t) => Int -> Maybe t
toEnumIfInBounds i =
  let r = toEnum i
      max' = maxBound `asTypeOf` r
      min' = minBound `asTypeOf` r
   in if i >= fromEnum min' && i <= fromEnum max'
        then Just r
        else Nothing

map4Tuple :: (a -> b) -> (a, a, a, a) -> (b, b, b, b)
map4Tuple f (w, x, y, z) = (f w, f x, f y, f z)

map5Tuple :: (a -> b) -> (a, a, a, a, a) -> (b, b, b, b, b)
map5Tuple f (v, w, x, y, z) = (f v, f w, f x, f y, f z)

-- | Infinite sequence [x, f x, f $ f x, f $ f $ f x, ...]
succForever :: Endo a -> a -> [a]
succForever (Endo endoFunc) !initX = initX : go endoFunc initX
  where
    go f !x =
      let !next = f x
       in next : go f next

{-# WARNING
debugWrtLn "Remove ASAP (debugging only)"
 #-}

debugWrtLn :: Show a => a -> a
debugWrtLn x = trace ("~~~ " <> show x <> " ~~~") x

noUnderscoreAndOmitNothingFields :: A.Options
noUnderscoreAndOmitNothingFields =
  A.defaultOptions {A.fieldLabelModifier = drop 1, A.omitNothingFields = True}
