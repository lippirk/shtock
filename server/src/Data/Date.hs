{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.Date
  ( DateN
  , mkDateRange
  , parseDate
  , encodeDate
  , H.Month(..)
  , mkDateN
  , _unDateN
  ) where

import qualified Data.Aeson       as A
import qualified Data.Aeson.Types as AT
import qualified Data.Hourglass   as H
import qualified Data.Newtypes    as N
import qualified Data.Text        as T

newtype DateN = DateN
  { _unDateN :: H.Date
  } deriving (Eq, Ord)

-- | Generate all dates (inclusively) between (start,end)
mkDateRange :: (DateN, DateN) -> [DateN]
mkDateRange (startDate, endDate) =
  if endDate < startDate
    then []
    else takeWhile (<= endDate) $ succForever (Endo getTomorrow) startDate
  where
    getTomorrow =
      DateN .
      flip
        H.dateAddPeriod
        (H.Period {H.periodYears = 0, H.periodMonths = 0, H.periodDays = 1}) .
      _unDateN

instance Show DateN where
  show (DateN (H.Date y m d)) = show y <> " " <> show m <> " " <> show d

mkDateN :: N.Nat -> H.Month -> N.Nat -> Maybe DateN
mkDateN (N.Nat year) month (N.Nat day) =
  if isDayValid year month day
    then Just $ DateN $ H.Date year month day
    else Nothing
  where
    isDayValid _ H.January d = d <= 31
    isDayValid y H.February d =
      if y `mod` 4 == 0
        then d <= 29
        else d <= 28
    isDayValid _ H.March d = d <= 31
    isDayValid _ H.April d = d <= 30
    isDayValid _ H.May d = d <= 31
    isDayValid _ H.June d = d <= 30
    isDayValid _ H.July d = d <= 31
    isDayValid _ H.August d = d <= 31
    isDayValid _ H.September d = d <= 30
    isDayValid _ H.October d = d <= 31
    isDayValid _ H.November d = d <= 30
    isDayValid _ H.December d = d <= 31

parseDate :: Text -> Maybe DateN
parseDate date' =
  let numbers :: [Int] = mapMaybe readMaybe $ T.splitOn "-" date'
   in case numbers of
        [y, m, d] ->
          DateN <$> (H.Date y <$> toEnumIfInBounds (m - 1) <*> pure d)
        _ -> Nothing

encodeDate :: DateN -> Text
encodeDate (DateN (H.Date y m d)) =
  tshow y <> "-" <> showSmallNat (fromEnum m + 1) <> "-" <> showSmallNat d
  where
    showSmallNat n -- i.e. 0 <  n < 99
     =
      if n < 10
        then "0" <> tshow n
        else tshow n

instance A.FromJSON DateN where
  parseJSON (A.String t) = maybe err pure (parseDate t)
    where
      err = (fail . unpack) ("Can't parse date: " <> t)
  parseJSON invalid = AT.typeMismatch "Date" invalid

instance A.ToJSON DateN where
  toJSON = A.String . encodeDate
