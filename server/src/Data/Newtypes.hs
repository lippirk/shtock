{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables        #-}

-- | Don't really want to export constructors for newtypes, but it is
-- too convenient. If this becomes an issue, use two modules?
--
--   TODO: Consider looking into GADTs to represent Nat + Vec
module Data.Newtypes
  ( Nat(..)
  , mkNat
  ) where

import qualified Data.Aeson as A

--------------------------------------------------------------------------------
-- | Positive Ints
newtype Nat = Nat
  { _unNat :: Int
  } deriving (Eq, Show, A.ToJSON, Additive)

instance A.FromJSON Nat where
  parseJSON v = do
    int :: Int <- A.parseJSON v
    case mkNat int of
      Nothing -> fail "Value is not positive."
      Just n  -> return n

mkNat :: Int -> Maybe Nat
mkNat x =
  if x > 0
    then Just (Nat x)
    else Nothing
