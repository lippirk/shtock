{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE RankNTypes                 #-}

module Data.Vec
  ( Vec
  , align
  , sizing
  , withVec
  , unsafeFromVector
  , subtract
  , zipWith
  , zip
  , postscanl'
  ) where

import qualified Data.Vector as V
import           GDP         (The)

newtype Vec n a =
  Vec (Vector a)
  deriving (Functor, Eq, Show)

instance The (Vec n a) (Vector a)

align :: Vec n a -> Vector b -> Maybe (Vec n b)
align xs ys =
  if V.length (the xs) == V.length ys
    then Just (Vec ys)
    else Nothing

sizing :: Vector a -> (forall n. Vec n a -> b) -> b
sizing xs f = f (Vec xs)

withVec :: (forall n. Vec n a -> b) -> Vector a -> b
withVec f xs = f (Vec xs)

{-# SPECIALISE subtract ::
                 Vec n Double -> Vec n Double -> Vec n Double #-}

subtract :: Subtractive a => Vec n a -> Vec n a -> Vec n (Difference a)
subtract = zipWith (-)

{-# INLINE zip #-}
zip :: Vec n a -> Vec n b -> Vec n (a, b)
zip xs ys = Vec $ V.zip (the xs) (the ys)

{-# INLINE zipWith #-}
zipWith :: (a -> b -> c) -> Vec n a -> Vec n b -> Vec n c
zipWith f xs ys = Vec $ V.zipWith f (the xs) (the ys)

{-# INLINE postscanl' #-}
postscanl' :: (a -> b -> a) -> a -> Vec n b -> Vec n a
postscanl' acc init' xs = Vec $ V.postscanl' acc init' (the xs)

unsafeFromVector :: forall n a. Vector a -> Vec n a
unsafeFromVector = Vec
