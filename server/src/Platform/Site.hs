{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE ConstraintKinds     #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Platform.Site where

import qualified Core.Response           as R
import qualified Data.ByteString.Builder as BSB
import qualified Iex.Api                 as Iex
import qualified Iex.Types               as Iex
import           Snap.Core               (Snap, ifTop, route)
import qualified Snap.Core               as S
import qualified Snap.Util.FileServe     as SFS
import qualified System.IO.Streams       as Streams

site :: Iex.Effects -> Snap ()
site effs =
  ifTop (SFS.serveFile "../ts-client/build/index.html") <|>
  route
    [ ( "/"
      , SFS.serveDirectoryWith SFS.defaultDirectoryConfig "../ts-client/build/")
    , ("stock/chart/", stockChartHandler)
    , ("stock/strategy/", stockStrategyHandler)
    ]
  where
    stockChartHandler = do
      params <- S.getQueryParams
      respondJSON $ Iex.renderResponse effs params
    ----------------------------------------------------------------------------
    stockStrategyHandler = do
      params <- S.getQueryParams
      respondJSON $ Iex.renderStrategyResponse effs params
    ----------------------------------------------------------------------------
    respondJSON f = do
      (!code, !json) <- liftIO $ R._unResponseJSON <$> f
      -- evaluating `json` to WHNF helps avoid hidden exceptions in pure code
      S.putResponse <$>
        setResponseBody json .
        S.setContentType "application/json" . S.setResponseCode code $
        S.emptyResponse
    ----------------------------------------------------------------------------
    setResponseBody :: LByteString -> S.Response -> S.Response
    setResponseBody json =
      S.setResponseBody
        (\out -> do
           Streams.write (Just $ BSB.lazyByteString json) out
           return out)
