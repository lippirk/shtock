{-# OPTIONS_GHC -fno-warn-missing-import-lists  #-}

-- | Wrappers around Math functions
module Iex.Indicators
  ( maximumClosePrice
  , closePriceSMA
  , closePriceMACD
  ) where

import qualified Data.Newtypes   as N
import           Data.Vec        (Vec)
import qualified Data.Vector     as V
import           Iex.Types
import qualified Math.Indicators as Ind

maximumClosePrice :: Vec n DailyPrice -> Double
maximumClosePrice xs =
  V.foldl' (\curr next -> max curr (_close next)) (-1 :: Double) (the xs)

closePriceSMA :: N.Nat -> Vec n DailyPrice -> Vec n Double
closePriceSMA n = Ind.simpleMovingAverage n . fmap _close

closePriceMACD :: Vec n DailyPrice -> Vec n Double
closePriceMACD = Ind.macd . fmap _close
