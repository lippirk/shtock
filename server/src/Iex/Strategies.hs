{-# LANGUAGE BangPatterns           #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists  #-}

module Iex.Strategies
  ( StrategyData(..)
  , name
  , historicPerformance
  , Strategy(..)
  , calcCapitals
  , calcSmaStrategies
  , buyStrategy
  , sellStrategy
  ) where

import           Control.Lens         ((^.))
import qualified Control.Lens.TH      as L
import           Control.Lens.Tuple   (_3)
import qualified Data.Aeson           as A
import qualified Data.Newtypes        as N
import           Data.Vec             (Vec, postscanl', unsafeFromVector, zip)
import qualified Data.Vector          as V
import           Foundation.Numerical (roundDown)
import           Iex.Indicators
import           Iex.Types

-- | TODO think about extending to encorporate richer strategies. e.g. sell 50%
data Strategy
  = Keep
  | Buy
  | Sell
  deriving (Eq, Show, Generic)

data StrategyData = StrategyData
  { _name                :: !Text
  , _historicPerformance :: Vector Double
  } deriving (Eq, Show, Generic)

L.makeFieldsNoPrefix ''StrategyData

instance A.ToJSON StrategyData where
  toJSON = A.genericToJSON noUnderscoreAndOmitNothingFields

instance A.FromJSON StrategyData where
  parseJSON = A.genericParseJSON noUnderscoreAndOmitNothingFields

type Capital = Double

type SharePrice = Double

type NumberOfShares = Int

-- | Calculate how your capital changes over time if you were to invest it using
--   a certain strategy, in a particular stock.
--   NB. Assumes you want to either have all of your money invested or none.
--       Doesn't take into account any fees (TODO).
calcCapitals :: Capital -> Vec n DailyPrice -> Vec n Strategy -> Vec n Capital
calcCapitals !initCapital !dps !strategies =
  fmap
    (\(closePrice, numberOfShares, cash) ->
       closePrice * fromIntegral numberOfShares + cash) $
  postscanl' acc (V.head (the dps) ^. close, 0, initCapital) $
  zip dps strategies
  where
    acc (!_, !prevNumberOfShares, !prevCash) (DailyPrice !_ !closePrice, !strat) =
      case strat of
        Keep -> (closePrice, prevNumberOfShares, prevCash)
        Buy ->
          let (nextNumShares, spareCash) = buyShares prevCash closePrice
           in (closePrice, prevNumberOfShares + nextNumShares, spareCash)
        Sell ->
          (closePrice, 0, prevCash + sellShares prevNumberOfShares closePrice)
    ----------------------------------------------------------------------------
    buyShares :: Capital -> SharePrice -> (NumberOfShares, Capital)
    buyShares !cash !sharePrice =
      let numberOfSharesCanBuy = roundDown (cash / sharePrice)
       in ( numberOfSharesCanBuy
          , cash - sharePrice * fromIntegral numberOfSharesCanBuy)
    ----------------------------------------------------------------------------
    sellShares :: NumberOfShares -> SharePrice -> Capital
    sellShares !numberOfShares !sharePrice =
      fromIntegral numberOfShares * sharePrice

-- | Calculate a long term moving average, and a short term moving average.
--   When the short crosses above the long, buy.
--   When the long crosses below the short, sell.
--   https://www.investopedia.com/articles/active-trading/052014/how-use-moving-average-buy-stocks.asp
--
--   TODO eliminiate overcomputation (don't need to calculate first 89 SMAs)
calcSmaStrategies :: Vec n DailyPrice -> Vec n Strategy
calcSmaStrategies dps =
  if len <= 90
    then unsafeFromVector $ V.replicate len Keep
    else unsafeFromVector $
         V.replicate 90 Keep <>
         ((^. _3) <$>
          V.postscanl'
            acc
            (the shortSMA V.! 89, the longSMA V.! 89, Keep)
            shortAndLong)
  where
    acc (!prevShort, !prevLong, _) (!nextShort, !nextLong) =
      let prevDoingWell = prevShort > prevLong
          nextDoingWell = nextShort > nextLong
       in (,,) nextShort nextLong $
          case (prevDoingWell, nextDoingWell) of
            (True, False) -> Sell
            (False, True) -> Buy
            _             -> Keep
    ----------------------------------------------------------------------------
    shortAndLong =
      V.zip
        (V.slice 90 (len - 90) (the shortSMA))
        (V.slice 90 (len - 90) (the longSMA))
    ----------------------------------------------------------------------------
    longSMA = closePriceSMA (N.Nat 90) dps
    ----------------------------------------------------------------------------
    shortSMA = closePriceSMA (N.Nat 30) dps
    ----------------------------------------------------------------------------
    len = V.length (the dps)

--------------------------------------------------------------------------------
buyStrategy :: Vec n DailyPrice -> Vec n Strategy
buyStrategy = fmap (const Buy)

sellStrategy :: Vec n DailyPrice -> Vec n Strategy
sellStrategy = fmap (const Sell)
