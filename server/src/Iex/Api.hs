{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists  #-}

module Iex.Api
  ( renderResponse
  , mkChartRequest__
  , mkStrategyRequest__
  , renderStrategyResponse
  ) where

import           Control.Lens                 ((^.))
import qualified Core.Response                as R
import qualified Data.Aeson                   as A
import qualified Data.ByteString              as BS
import qualified Data.ByteString.Lazy         as LBS
import qualified Data.Date                    as D
import qualified Data.Map.Strict              as MS
import qualified Data.Newtypes                as N
import qualified Data.Text                    as T
import qualified Data.Text.Encoding           as TE
import           Data.Vec                     (sizing)
import qualified Data.Vector                  as V
import qualified Data.Vector.Algorithms.Intro as VI
import           Iex.Indicators
import           Iex.Strategies
import           Iex.Types
import qualified Network.HTTP.Client          as HC

--------------------------------------------------------------------------------
renderStrategyResponse :: Effects -> Request -> IO R.ResponseJSON
renderStrategyResponse effs r = do
  eDps <- try $ getStrategyResponse effs r
  return $
    case eDps of
      Left e          -> R.errorToJSON $ R.ResponseError (getErrorCode e) e
      Right stratData -> R.successToJSON $ R.ResponseSuccess 200 stratData

renderResponse :: Effects -> Request -> IO R.ResponseJSON
renderResponse effs r = do
  eChart <- try $ getChart effs r
  return $
    case eChart of
      Left e      -> R.errorToJSON $ R.ResponseError (getErrorCode e) e
      Right chart -> R.successToJSON $ R.ResponseSuccess 200 chart

getStrategyResponse :: Effects -> Request -> IO StrategyData
getStrategyResponse effs r = do
  edailyPrices <- (effs ^. mkStrategyRequest) r
  dailyPrices <- liftEither (checkNotEmptyAndSort =<< edailyPrices)
  sizing dailyPrices $ \dps -> do
    let histPerf = calcCapitals 1000 dps (calcSmaStrategies dps)
    return $ StrategyData "sma" (the histPerf)

getChart :: Effects -> Request -> IO Chart
getChart effs rq = do
  q@(ChartQuery ticker r _) <- liftEither $ parseChartQuery rq
  eprices <- effs ^. mkChartRequest $ q
  dailyPrices <-
    liftEither $ trimDailyPrices r =<< checkNotEmptyAndSort =<< eprices
  return $
    sizing dailyPrices $ \dps -> do
      let maxPrice = maximumClosePrice dps
          sma = closePriceSMA (N.Nat 5) dps
          macd = closePriceMACD dps
          chartData =
            ChartData
              { _dailyPrices = the dps
              , _maximum = Just maxPrice
              , _sma = Just (the sma)
              , _macd = Just (the macd)
              }
      Chart {_ticker = ticker, _chartData = chartData}

liftEither :: Exception e => Either e s -> IO s
liftEither lOrR =
  case lOrR of
    Left l  -> throwM l
    Right r -> return r

--------------------------------------------------------------------------------
trimDailyPrices :: Range -> Vector DailyPrice -> Either Err (Vector DailyPrice)
trimDailyPrices = betweenDates id
  where
    betweenDates ::
         (Vector DailyPrice -> a) -> Range -> Vector DailyPrice -> Either Err a
    betweenDates f r prices = do
      (start, end) <- startEndIndices r
      return $ f $ V.slice start (end - start + 1) prices
      where
        startEndIndices (RangeBetween startDate endDate) = do
          start <-
            maybeToEither
              (DataErr $
               "Could not find any data on or after the start date: " <>
               tshow startDate)
              mstart
          end <-
            maybeToEither
              (DataErr $
               "Could not find any data on or before the end date: " <>
               tshow endDate)
              mend
          if end <= start
            then Left $
                 DataErr $
                 "Start date (" <> tshow ((prices V.! start) ^. date) <>
                 ") should occur before end date (" <>
                 tshow ((prices V.! end) ^. date) <>
                 "). This error might have occured there was no market data available on the start and end dates you requested (" <>
                 tshow startDate <>
                 ", " <>
                 tshow endDate <>
                 " respectively), so the closest possible days were chosen."
            else return (start, end)
          where
            mstart = V.findIndex (\p -> p ^. date >= startDate) prices
            -- `V.reverse` doesn't fuse well apparently :(
            mend =
              ((V.length prices - 1) -) <$>
              V.findIndex (\p -> p ^. date <= endDate) (V.reverse prices)
        startEndIndices _ = return (0, V.length prices - 1)

--------------------------------------------------------------------------------
-- | Effect implementation
mkChartRequest__ ::
     HC.Manager -> ChartQuery -> IO (Either Err (Vector DailyPrice))
mkChartRequest__ m q = do
  ebsJson <- requestJson
  case ebsJson of
    Left e -> return $ Left e
    Right bsJson ->
      return $
      case A.eitherDecode' bsJson of
        Left errStr -> Left $ ParseErr $ T.pack errStr
        Right v     -> Right v
  where
    requestJson :: IO (Either Err LBS.ByteString)
    requestJson = do
      let rq =
            mkRq $
            TE.encodeUtf8 $ "stock/" <> q ^. symbol <> "/chart/" <> rangeParam
          rangeParam =
            case q ^. range of
              Range5Years -> "5y/"
              _           -> ""
      eResp <- try $ HC.httpLbs rq m
      case eResp of
        Left (e :: HC.HttpException) -> return $ Left $ NetworkErr (tshow e)
        Right r                      -> return $ Right $ HC.responseBody r

mkStrategyRequest__ ::
     HC.Manager -> Request -> IO (Either Err (Vector DailyPrice))
mkStrategyRequest__ m r =
  case TE.decodeUtf8 <$> (safeHead =<< MS.lookup "symbol" r) of
    Nothing -> throwM $ ParseErr "expected 'symbol'"
    Just ticker -> do
      ebsJson <- requestJson ticker
      case ebsJson of
        Left e -> return $ Left e
        Right bsJson ->
          return $
          case A.eitherDecode' bsJson of
            Left errStr -> Left $ ParseErr $ T.pack errStr
            Right v     -> Right v
  where
    requestJson :: Text -> IO (Either Err LBS.ByteString)
    requestJson ticker = do
      let rq = mkRq $ TE.encodeUtf8 $ "stock/" <> ticker <> "/chart/" <> "5y"
      -- add range (calculate from date range)
      eResp <- try $ HC.httpLbs rq m
      case eResp of
        Left (e :: HC.HttpException) -> return $ Left $ NetworkErr (tshow e)
        Right body                   -> return $ Right $ HC.responseBody body

mkRq :: BS.ByteString -> HC.Request
mkRq s =
  let baseRq = HC.parseRequest_ "https://api.iextrading.com/1.0/"
   in baseRq {HC.path = HC.path baseRq <> s}

checkNotEmptyAndSort :: Vector DailyPrice -> Either Err (Vector DailyPrice)
checkNotEmptyAndSort xs =
  V.modify (VI.sortBy $ comparing (_date :: DailyPrice -> D.DateN)) <$>
  (if V.null xs
     then Left $ DataErr "Empty data!"
     else Right xs)
