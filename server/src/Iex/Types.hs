{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE FunctionalDependencies     #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}

module Iex.Types
  ( Request
  -- DailyPrice
  , DailyPrice(..)
  , date
  , close
  -- Range
  , Range(..)
  , _RangeDefault
  -- ChartQuery
  , ChartQuery(..)
  , parseChartQuery
  , range
  , indicator
  , symbol
  -- Err
  , Err(..)
  , _QueryErr
  , getErrorCode
  -- Indicator
  , Indicator(..)
  -- effects
  , Effects(..)
  , mkChartRequest
  , mkStrategyRequest
  -- responses
  , Chart(..)
  , ChartData(..)
  ) where

import           Control.Lens.TH     (makeFieldsNoPrefix, makePrisms)
import qualified Data.Aeson          as A
import qualified Data.Date           as D
import qualified Data.HashMap.Strict as HMS
import qualified Data.Map.Strict     as MS
import qualified Data.Newtypes       as N
import qualified Data.Text           as T
import qualified Data.Text.Encoding  as TE

--------------------------------------------------------------------------------
-- | Some fields are not used (yet)
data DailyPrice = DailyPrice
  { _date  :: !D.DateN
  -- , _open   :: !Double
  -- , _high   :: !Double
  -- , _low    :: !Double
  , _close :: !Double
  -- , _volume :: !Int
  -- , _unadjustedVolume :: !Int
  -- , _change           :: !Double
  -- , _changePercent    :: !Double
  -- , _vwap             :: !Double
  -- , _changeOverTime   :: !Double
  } deriving (Eq, Show, Generic)

instance A.ToJSON DailyPrice where
  toJSON = A.genericToJSON noUnderscoreAndOmitNothingFields

instance A.FromJSON DailyPrice where
  parseJSON = A.genericParseJSON noUnderscoreAndOmitNothingFields

makeFieldsNoPrefix ''DailyPrice

--------------------------------------------------------------------------------
data Indicator
  = Maximum
  | SimpleMovingAverage !N.Nat
  | MACD
  deriving (Eq, Show)

--------------------------------------------------------------------------------
data Range
  = RangeBetween D.DateN
                 D.DateN
  | Range5Years
  | RangeDefault -- Represents default which the Iex api uses
                 -- and is (currently) 1 month
  deriving (Eq, Show)

makePrisms ''Range

--------------------------------------------------------------------------------
data ChartQuery = ChartQuery
  { _symbol    :: !Text
  , _range     :: !Range
  , _indicator :: !Indicator
  } deriving (Eq, Show)

makeFieldsNoPrefix ''ChartQuery

--------------------------------------------------------------------------------
data Chart = Chart
  { _ticker    :: !Text
  , _chartData :: !ChartData
  } deriving (Eq, Show)

instance A.ToJSON Chart where
  toJSON c =
    A.object
      ["type" A..= A.String "chart", _ticker c A..= A.toJSON (_chartData c)]

-- | TODO: Extend to parse multiple charts at once
instance A.FromJSON Chart where
  parseJSON =
    A.withObject "charts" $ \o -> do
      type' :: Text <- o A..: "type"
      let chartObjs = HMS.toList (HMS.delete "type" o)
      charts <- mapM parseChart chartObjs
      chart <-
        case safeHead charts of
          Just chart -> return chart
          _          -> fail "Expected exactly one chart"
      case type' of
        "chart" -> return chart
        _       -> fail "Expected type 'chart'"
    where
      parseChart (ticker, value) = do
        chartData <- A.parseJSON value
        return $ Chart ticker chartData

--------------------------------------------------------------------------------
data ChartData = ChartData
  { _dailyPrices :: !(Vector DailyPrice)
  , _maximum     :: !(Maybe Double)
  , _sma         :: !(Maybe (Vector Double))
  , _macd        :: !(Maybe (Vector Double))
  } deriving (Eq, Show, Generic)

instance A.ToJSON ChartData where
  toJSON = A.genericToJSON noUnderscoreAndOmitNothingFields

instance A.FromJSON ChartData where
  parseJSON = A.genericParseJSON noUnderscoreAndOmitNothingFields

--------------------------------------------------------------------------------
data Err
  = NetworkErr !Text
  | QueryErr !Text
  | ParseErr !Text
  | CalculationErr !Text
  | DataErr !Text
  deriving (Show, Generic)

instance Exception Err

instance A.ToJSON Err

instance A.FromJSON Err

makePrisms ''Err

getErrorCode :: Err -> Int
getErrorCode (NetworkErr _)     = 503
getErrorCode (QueryErr _)       = 400
getErrorCode (ParseErr _)       = 520
getErrorCode (CalculationErr _) = 520
getErrorCode (DataErr _)        = 520

--------------------------------------------------------------------------------
-- | Parameters from http request
type Request = MS.Map ByteString [ByteString]

--------------------------------------------------------------------------------
data Effects = Effects
  { _mkChartRequest    :: ChartQuery -> IO (Either Err (Vector DailyPrice))
  , _mkStrategyRequest :: Request -> IO (Either Err (Vector DailyPrice))
  }

makeFieldsNoPrefix ''Effects

--------------------------------------------------------------------------------
-- | Determines how http requests are turned into ChartQuery's
parseChartQuery :: Request -> Either Err ChartQuery
parseChartQuery ps = ChartQuery <$> esymbol <*> erange <*> eindicator
  where
    (msymbol, mstartDateText, mendDateText, mindicator, mrange) =
      map5Tuple
        getParamAsText
        ("symbol", "start-date", "end-date", "indicator", "range")
    ----------------------------------------------------------------------------
    esymbol = maybeToEither (QueryErr "No stock symbol provided") msymbol
    ----------------------------------------------------------------------------
    eindicator =
      case mindicator of
        Nothing -> Left $ QueryErr "No 'indicator' parameter"
        Just ind ->
          maybeToEither
            (QueryErr $ "Unknown indicator: " <> T.pack (show ind))
            (parseIndicator ind)
    ----------------------------------------------------------------------------
    erange = do
      mstartDate <- estartDate
      mendDate <- eendDate
      let mBetween = RangeBetween <$> mstartDate <*> mendDate
      case mBetween of
        Just between -> Right between
        Nothing ->
          Right $
          case mrange of
            Just "5y" -> Range5Years
            _         -> RangeDefault
    ----------------------------------------------------------------------------
    estartDate =
      case mstartDateText of
        Nothing -> return Nothing
        Just startDate ->
          case D.parseDate startDate of
            Nothing ->
              Left $ QueryErr $ "Could not parse start-date: " <> startDate
            Just d -> return $ Just d
    ----------------------------------------------------------------------------
    eendDate =
      case mendDateText of
        Nothing -> return Nothing
        Just endDate ->
          case D.parseDate endDate of
            Nothing ->
              Left $ QueryErr $ "Could not parse start-date: " <> endDate
            Just d -> return $ Just d
    ----------------------------------------------------------------------------
    getParamAsText :: ByteString -> Maybe Text
    getParamAsText key = TE.decodeUtf8 <$> (safeHead =<< MS.lookup key ps)
    ----------------------------------------------------------------------------
    parseIndicator "maximum" = Just Maximum
    parseIndicator "simple-moving-average" =
      Just $ SimpleMovingAverage (N.Nat 5)
    parseIndicator "macd" = Just MACD
    parseIndicator _ = Nothing
