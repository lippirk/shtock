module Core.Env
  ( withEnv
  , App
  , Env(..)
  ) where

import qualified Network.HTTP.Client     as HC
import qualified Network.HTTP.Client.TLS as HCT
import qualified Snap.Http.Server.Config as SC

type App = ReaderT Env IO

data Env = Env
  { _logConfig   :: !SC.ConfigLog
  , _httpManager :: !HC.Manager
  }

withEnv :: (Env -> IO a) -> IO a
withEnv f = do
  let logConfig = SC.ConfigIoLog (print . show)
  manager <- HCT.newTlsManager
  f $ Env logConfig manager
