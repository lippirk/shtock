{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}

module Core.Response
  ( ResponseSuccess(..)
  , code
  , payload
  , ResponseError(..)
  , StatusCode
  , errorToJSON
  , successToJSON
  , ResponseJSON
  , _unResponseJSON
  ) where

import qualified Control.Lens.TH as L
import qualified Data.Aeson      as A

--------------------------------------------------------------------------------
data ResponseError e = ResponseError
  { _code  :: !Int
  , _error :: !e
  } deriving (Eq, Show)

instance A.ToJSON e => A.ToJSON (ResponseError e) where
  toJSON (ResponseError c error) =
    A.object
      [ "status" A..= A.String "error"
      , "code" A..= A.toJSON c
      , "error" A..= A.toJSON error
      ]

instance A.FromJSON e => A.FromJSON (ResponseError e) where
  parseJSON =
    A.withObject "responseError" $ \o -> do
      s :: Text <- o A..: "status"
      c :: Int <- o A..: "code"
      case s of
        "error" -> do
          error :: p <- o A..: "error"
          return $ ResponseError c error
        _ -> fail "Expected status: 'error'"

--------------------------------------------------------------------------------
data ResponseSuccess p = ResponseSuccess
  { _code    :: !Int
  , _payload :: !p
  } deriving (Eq, Show)

L.makeFieldsNoPrefix ''ResponseSuccess

instance A.ToJSON p => A.ToJSON (ResponseSuccess p) where
  toJSON (ResponseSuccess c p) =
    A.object
      [ "status" A..= A.String "ok"
      , "code" A..= A.toJSON c
      , "result" A..= A.toJSON p
      ]

instance A.FromJSON p => A.FromJSON (ResponseSuccess p) where
  parseJSON =
    A.withObject "responseSuccess" $ \o -> do
      s :: Text <- o A..: "status"
      c :: Int <- o A..: "code"
      case s of
        "ok" -> do
          result :: p <- o A..: "result"
          return $ ResponseSuccess c result
        _ -> fail "Expected status: 'ok'"

--------------------------------------------------------------------------------
type StatusCode = Int

-- | Final data structure before response is sent over the wire
newtype ResponseJSON = ResponseJSON
  { _unResponseJSON :: (StatusCode, LByteString)
  } deriving (Eq, Show)

successToJSON :: A.ToJSON a => ResponseSuccess a -> ResponseJSON
successToJSON success@(ResponseSuccess c _) = ResponseJSON (c, A.encode success)

errorToJSON :: A.ToJSON a => ResponseError a -> ResponseJSON
errorToJSON error@(ResponseError c _) = ResponseJSON (c, A.encode error)
