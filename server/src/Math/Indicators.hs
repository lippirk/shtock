module Math.Indicators
  ( simpleMovingAverage
  , exponentialMovingAverage
  , macd
  ) where

import qualified Data.Newtypes as N
import           Data.Vec      (Vec, postscanl', subtract, unsafeFromVector)
import qualified Data.Vector   as V

--------------------------------------------------------------------------------
simpleMovingAverage :: N.Nat -> Vec n Double -> Vec n Double
simpleMovingAverage (N.Nat n) xs =
  unsafeFromVector $ averagedFirstN V.++ averagedRest
  where
    averagedFirstN =
      V.drop 1 $
      V.iscanl'
        (\idx prev next ->
           let i = fromIntegral idx
            in (i * prev + next) / (i + 1))
        0
        firstN
    averagedRest =
      V.drop 1 $
      V.iscanl'
        (\idx prev next -> prev + (next - the xs V.! idx) / fromIntegral n)
        (V.last averagedFirstN)
        rest
    (firstN, rest) = V.splitAt n (the xs)

--------------------------------------------------------------------------------
macd :: Vec n Double -> Vec n Double
macd xs =
  exponentialMovingAverage (2 / (12 + 1)) xs `subtract`
  exponentialMovingAverage (2 / (26 + 1)) xs

-- | ema_1 = x_1
--   ema_n = alpha . x_n + (1-alpha) . ema_(n-1)
exponentialMovingAverage :: Double -> Vec n Double -> Vec n Double
exponentialMovingAverage alpha xs = postscanl' acc (V.head (the xs)) xs
  where
    acc ema_n next = alpha * next + (1 - alpha) * ema_n
