{-# LANGUAGE ScopedTypeVariables  #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE UndecidableInstances #-}

module Init
  ( runApp
  ) where

import qualified Iex.Api       as Iex
import qualified Iex.Types     as Iex

import           Core.Env      (Env)
import qualified Core.Env      as E
import           Platform.Site (site)
import qualified Snap          as S

--------------------------------------------------------------------------------
runApp :: IO ()
runApp = E.withEnv runServer
  where
    runServer :: Env -> IO ()
    runServer e@(E.Env logConfig _) = S.httpServe config site'
      where
        site' = site (iexEffects e)
        config =
          S.setErrorLog logConfig . S.setAccessLog logConfig $ S.defaultConfig

--------------------------------------------------------------------------------
iexEffects :: Env -> Iex.Effects
iexEffects (E.Env _ httpManager) =
  Iex.Effects
    { Iex._mkChartRequest = Iex.mkChartRequest__ httpManager
    , Iex._mkStrategyRequest = Iex.mkStrategyRequest__ httpManager
    }
