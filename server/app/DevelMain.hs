{-# LANGUAGE OverloadedStrings #-}

-- | Running your app inside GHCi.
--
-- > stack ghci
--
-- To start your app, run:
--
-- > :l DevelMain
-- > DevelMain.update
--
-- You can also call @DevelMain.shutdown@ to stop the app
--
-- There is more information about this approach,
-- on the wiki: https://github.com/yesodweb/yesod/wiki/ghci
module DevelMain where

import           Control.Concurrent (MVar, ThreadId, forkIO, killThread,
                                     newEmptyMVar, putMVar, takeMVar)
import           Control.Exception  (finally)
import           Control.Monad      ((>=>))
import           Data.IORef         (IORef, newIORef, readIORef, writeIORef)
import qualified Foreign.Store      as FS
import           GHC.Word           (Word32)
import           Init               (runApp)

-- | Start or restart the server.
-- newStore is from foreign-store.
-- A Store holds onto some data across ghci reloads
update :: IO ()
update = do
  mtidStore <- FS.lookupStore tidStoreNum
  case mtidStore
      -- no server running
        of
    Nothing -> do
      done <- FS.storeAction doneStore newEmptyMVar
      tid <- start done
      _ <- FS.storeAction (FS.Store tidStoreNum) (newIORef tid)
      return ()
      -- server is already running
    Just tidStore -> restartAppInNewThread tidStore
  where
    doneStore :: FS.Store (MVar ())
    doneStore = FS.Store 0
    -- shut the server down with killThread and wait for the done signal
    restartAppInNewThread :: FS.Store (IORef ThreadId) -> IO ()
    restartAppInNewThread tidStore =
      modifyStoredIORef tidStore $ \tid -> do
        killThread tid
        FS.withStore doneStore takeMVar
        FS.readStore doneStore >>= start
    -- | Start the server in a separate thread.
    start ::
         MVar () -- ^ Written to when the thread is killed.
      -> IO ThreadId
    start done =
      forkIO
        (finally
           runApp
                        -- Note that this implies concurrency
                        -- between shutdownApp and the next app that is starting.
                        -- Normally this should be fine
           (putMVar done ()))

-- | kill the server
shutdown :: IO ()
shutdown = do
  mtidStore <- FS.lookupStore tidStoreNum
  case mtidStore
      -- no server running
        of
    Nothing -> putStrLn "no app running"
    Just tidStore -> do
      FS.withStore tidStore $ readIORef >=> killThread
      putStrLn "App is shutdown"

tidStoreNum :: Word32
tidStoreNum = 1

modifyStoredIORef :: FS.Store (IORef a) -> (a -> IO a) -> IO ()
modifyStoredIORef store f =
  FS.withStore store $ \ref -> do
    v <- readIORef ref
    f v >>= writeIORef ref
