module Util
  ( unsafeMkDate
  , mkDailyPrices
  , genFixedLengthVector
  , unsafeFromList
  , nonEmptyVector
  ) where

import           Control.Monad   (replicateM)
import qualified Data.Date       as D
import qualified Data.List       as L
import qualified Data.Maybe      as Maybe
import qualified Data.Newtypes   as N
import           Data.Vec        (Vec, unsafeFromVector)
import qualified Data.Vector     as V
import qualified Iex.Types       as Iex
import           Instances       ()
import           Test.QuickCheck

unsafeMkDate :: (Int, D.Month, Int) -> D.DateN
unsafeMkDate (y, m, d) = Maybe.fromJust (D.mkDateN (N.Nat y) m (N.Nat d))

mkDailyPrices :: (D.DateN, D.DateN) -> [Double] -> Vector Iex.DailyPrice
mkDailyPrices startAndEndDate closePrices =
  V.fromList $ uncurry Iex.DailyPrice <$> L.zip dates closePrices
  where
    dates = D.mkDateRange startAndEndDate

genFixedLengthVector :: Arbitrary a => Int -> Gen (Vector a)
genFixedLengthVector size = V.fromList . L.take size <$> infiniteList

unsafeFromList :: [a] -> Vec n a
unsafeFromList = unsafeFromVector . V.fromList

nonEmptyVector :: Arbitrary a => Gen (Vector a)
nonEmptyVector = arbitrary `suchThat` (not . V.null)
