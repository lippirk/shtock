{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Spec.DateSpec
  ( spec
  ) where

import           Data.Date
import           Data.Hourglass  (Month (..))
import qualified Data.Newtypes   as N
import           Instances       ()
import           Test.Hspec
import           Test.QuickCheck
import           Util

spec = do
  describe "encoding date and then parsing it" $
    it "gives original date" $
    property $ \(d :: DateN) -> (parseDate . encodeDate) d `shouldBe` Just d
  describe "mkDateRange" $ do
    it "generates correct list of dates" $
      mkDateRange
        (unsafeMkDate (1999, January, 1), unsafeMkDate (1999, January, 3)) `shouldBe`
      [ unsafeMkDate (1999, January, 1)
      , unsafeMkDate (1999, January, 2)
      , unsafeMkDate (1999, January, 3)
      ]
    it "generates singleton if start date == end date" $
      mkDateRange
        (unsafeMkDate (2017, February, 28), unsafeMkDate (2017, February, 28)) `shouldBe`
      [unsafeMkDate (2017, February, 28)]
    it "generates empty list if end date before start date" $
      mkDateRange
        (unsafeMkDate (1999, January, 1), unsafeMkDate (1997, December, 2)) `shouldBe`
      []
