{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Spec.ResponseSpec
  ( spec
  ) where

import qualified Core.Response   as R
import qualified Data.Aeson      as A
import qualified Data.Maybe      as Maybe
import           Test.Hspec
import           Test.QuickCheck

spec :: Spec
spec =
  describe "Response" $ do
    describe "ResponseSuccess" $
      it "fromJust . decode . encode == id" $ do
        let orig = R.ResponseSuccess 200 (1 :: Int)
            actual = A.decode . A.encode $ orig
        actual `shouldBe` Just orig
    describe "ResponseError" $
      it "fromJust . decode . encode == id" $ do
        let orig = R.ResponseError 400 (-1 :: Int)
            actual = A.decode . A.encode $ orig
        actual `shouldBe` Just orig
