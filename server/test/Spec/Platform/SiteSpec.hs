{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Spec.Platform.SiteSpec where

import           Control.Lens        ((.~), (^.))
import qualified Core.Response       as R
import qualified Data.Aeson          as A
import qualified Data.Aeson.Types    as AT
import qualified Data.ByteString     as BS
import qualified Data.Date           as D
import qualified Data.HashMap.Strict as HMS
import qualified Data.Map            as M
import qualified Data.Maybe          as Maybe
import qualified Data.Newtypes       as N
import qualified Data.Vector         as V
import qualified Iex.Strategies      as Iex
import qualified Iex.Types           as Iex
import           Platform.Site       (site)
import qualified Snap.Core           as S
import           Snap.Test
import           Test.Hspec
import           Test.QuickCheck
import           Util

spec :: Spec
spec =
  describe "site" $ do
    describe "stock/strategy/" $ do
      describe "api failure" $
        describe "empty data" $
        it "gives error 520" $ do
          r <-
            runHandler
              (get "/stock/strategy/" $ M.fromList [("symbol", ["msft"])])
              (site $
               effs & Iex.mkStrategyRequest .~ (\_ -> return $ Right V.empty))
          S.rspStatus r `shouldBe` 520
      describe "valid request" $
        it "gives expected return on a sma strategy" $ do
          r <-
            runHandler
              (get "/stock/strategy/" $ M.fromList [("symbol", ["aapl"])])
              site'
          S.rspStatus r `shouldBe` 200
          Just (strategyData :: R.ResponseSuccess Iex.StrategyData) <-
            A.decodeStrict <$> getResponseBody r
          -- strategyData ^. R.payload . Iex.historicPerformance `shouldBe`
          --   V.fromList [1000, 1000, 1000]
          pendingWith "Write useful test of strategies"
    describe "stock/chart/" $ do
      describe "api failure" $
        describe "empty data" $
        it "gives error 520" $ do
          r <-
            runHandler
              (get "/stock/chart/" $
               M.fromList [("symbol", ["msft"]), ("indicator", ["maximum"])])
              (site $
               effs & Iex.mkChartRequest .~ (\_ -> return $ Right V.empty))
          S.rspStatus r `shouldBe` 520
      describe "invalid request" $ do
        describe "no indicator" $
          it "gives error 400" $ do
            r <-
              runHandler
                (get "/stock/chart/" $ M.fromList [("symbol", ["msft"])])
                site'
            S.rspStatus r `shouldBe` 400
        describe "unknown indicator" $
          it "gives error 400" $ do
            r <-
              runHandler
                (get "/stock/chart/" $
                 M.fromList [("symbol", ["msft"]), ("indicator", ["asdf"])])
                site'
            S.rspStatus r `shouldBe` 400
        describe "no symbol" $
          it "gives error 400" $ do
            r <-
              runHandler
                (get "/stock/chart/" $ M.fromList [("indicator", ["maximum"])])
                site'
            S.rspStatus r `shouldBe` 400
        describe "invalid start-date" $
          it "gives error 400" $ do
            r <-
              runHandler
                (get "/stock/chart/" $
                 M.fromList
                   [ ("symbol", ["msft"])
                   , ("indicator", ["maximum"])
                   , ("start-date", ["asdf"])
                   , ("end-date", ["2018-3-8"])
                   ])
                site'
            S.rspStatus r `shouldBe` 400
        describe "invalid end-date" $
          it "gives error 400" $ do
            r <-
              runHandler
                (get "/stock/chart/" $
                 M.fromList
                   [ ("symbol", ["msft"])
                   , ("indicator", ["maximum"])
                   , ("start-date", ["2018-3-5"])
                   , ("end-date", ["asdf"])
                   ])
                site'
            S.rspStatus r `shouldBe` 400
      describe "valid request" $ do
        describe "maximum indicator" $
          it "returns correct maximum" $ do
            r <-
              runHandler
                (get "/stock/chart/" $
                 M.fromList [("symbol", ["msft"]), ("indicator", ["maximum"])])
                site'
            S.rspStatus r `shouldBe` 200
            Just (chart :: R.ResponseSuccess Iex.Chart) <-
              A.decodeStrict <$> getResponseBody r
            let Just max' = Iex._maximum . Iex._chartData . R._payload $ chart
            max' `shouldBe` 111.75
        describe "start & end dates" $
          it "calculation performed between start & end dates" $ do
            r <-
              runHandler
                (get "/stock/chart/" $
                 M.fromList
                   [ ("symbol", ["msft"])
                   , ("indicator", ["maximum"])
                   , ("start-date", ["2018-3-7"])
                   , ("end-date", ["2018-3-8"])
                   ])
                site'
            S.rspStatus r `shouldBe` 200
            Just (chart :: R.ResponseSuccess Iex.Chart) <-
              A.decodeStrict <$> getResponseBody r
            let Just max' = Iex._maximum . Iex._chartData . R._payload $ chart
            max' `shouldBe` 110.51
        describe "sma" $
          it "returns correct sma" $ do
            r <-
              runHandler
                (get "/stock/chart/" $
                 M.fromList
                   [ ("symbol", ["msft"])
                   , ("indicator", ["simple-moving-average"])
                   ])
                site'
            S.rspStatus r `shouldBe` 200
            Just (chart :: R.ResponseSuccess Iex.Chart) <-
              A.decodeStrict <$> getResponseBody r
            let Just sma' = Iex._sma . Iex._chartData . R._payload $ chart
            sma' `shouldBe`
              V.fromList
                [111.75, (111.75 + 110.39) / 2, (111.75 + 110.39 + 110.51) / 3]
        describe "macd" $ it "returns correct macd" $ pendingWith "TODO"
  -- TODO test that returned close prices are in correct range
  -- TODO test case when start/end date is on a weekend

--------------------------------------------------------------------------------
getFakeChartData ::
     Iex.ChartQuery -> IO (Either Iex.Err (Vector Iex.DailyPrice))
getFakeChartData (Iex.ChartQuery "msft" _ _) =
  return $
  Right $
  V.fromList $
  fmap
    mkDailyPrice
    [ (2018, D.March, 6, 111.75)
    , (2018, D.March, 7, 110.39)
    , (2018, D.March, 8, 110.51)
    ]
getFakeChartData _ = undefined

-- | TODO Currently contains filler data
getFakeStrategyData ::
     Iex.Request -> IO (Either Iex.Err (Vector Iex.DailyPrice))
getFakeStrategyData r =
  return $
  Right $
  V.fromList $
  fmap
    mkDailyPrice
    [ (2018, D.March, 6, 111.75)
    , (2018, D.March, 7, 110.39)
    , (2018, D.March, 8, 110.51)
    ]

site' = site effs

effs =
  Iex.Effects
    { Iex._mkChartRequest = getFakeChartData
    , Iex._mkStrategyRequest = getFakeStrategyData
    }

mkDailyPrice :: (Int, D.Month, Int, Double) -> Iex.DailyPrice
mkDailyPrice (y, m, d, close) = Iex.DailyPrice (unsafeMkDate (y, m, d)) close
