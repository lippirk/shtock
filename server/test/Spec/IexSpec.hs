{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Spec.IexSpec
  ( spec
  ) where

import qualified Data.Aeson        as A
import qualified Data.Date         as D
import qualified Data.Maybe        as Maybe
import qualified Data.Newtypes     as N
import           Data.Vec          (Vec, unsafeFromVector)
import qualified Data.Vector       as V
import qualified Iex.Strategies    as Iex
import qualified Iex.Types         as Iex
import           Instances         ()
import           Test.Hspec
import           Test.QuickCheck
import qualified Text.RawString.QQ as Q
import           Util

spec :: Spec
spec = do
  describe "responses" $ do
    describe "maximum chart reponse" $ do
      let chart =
            Iex.Chart
              { Iex._ticker = "msft"
              , Iex._chartData =
                  Iex.ChartData
                    { Iex._dailyPrices =
                        V.fromList
                          [ Iex.DailyPrice
                              (unsafeMkDate (2019, D.January, 1))
                              110
                          ]
                    , Iex._maximum = Just 117
                    , Iex._sma = Nothing
                    , Iex._macd = Nothing
                    }
              }
      let json =
            [Q.r| {
                    "type": "chart",
                    "msft": {
                      "maximum": 117,
                      "dailyPrices": [
                        { "close": 110, "date": "2019-01-01" }
                      ]
                    }
                  } |]
      it "transforms from json" $ A.eitherDecode json `shouldBe` Right chart
      it "transforms to json and back" $
        A.eitherDecode (A.encode chart) `shouldBe` Right chart
    describe "sma + maximum + macd chart response" $ do
      let chart =
            Iex.Chart
              { Iex._ticker = "aapl"
              , Iex._chartData =
                  Iex.ChartData
                    { Iex._dailyPrices =
                        V.fromList
                          [ Iex.DailyPrice
                              (unsafeMkDate (2019, D.January, 1))
                              1111
                          , Iex.DailyPrice
                              (unsafeMkDate (2019, D.January, 2))
                              1112
                          ]
                    , Iex._maximum = Just 1121
                    , Iex._sma = Just $ V.fromList [1101, 1102]
                    , Iex._macd = Just $ V.fromList [8, 10]
                    }
              }
      let json =
            [Q.r| {
                    "type": "chart",
                    "aapl": {
                      "maximum": 1121,
                      "dailyPrices": [
                        { "close": 1111, "date": "2019-01-01" },
                        { "close": 1112, "date": "2019-01-02" }
                      ],
                      "sma": [1101, 1102],
                      "macd": [8, 10]
                    }
                  } |]
      it "transforms from json" $ A.eitherDecode json `shouldBe` Right chart
      it "transforms to json and back" $
        A.eitherDecode (A.encode chart) `shouldBe` Right chart
  describe "strategies" $ do
    describe "calcCapitals" $ do
      it "preserves length" $ do
        let dailyPricesAndStrategies = do
              len <- choose (1, 600)
              dailyPrices <- unsafeFromVector <$> genFixedLengthVector len
              strategies <- unsafeFromVector <$> genFixedLengthVector len
              return (dailyPrices, strategies, len)
        property $
          forAll dailyPricesAndStrategies $ \(dps, strts, len) ->
            len `shouldBe` V.length (the $ Iex.calcCapitals 1000 dps strts)
      describe "always keep" $
        it "capital stays the same" $ do
          let dailyPrices =
                unsafeFromVector $
                mkDailyPrices
                  ( unsafeMkDate (2019, D.January, 1)
                  , unsafeMkDate (2019, D.January, 3))
                  [110, 111, 117]
          let capitals =
                the $
                Iex.calcCapitals
                  100
                  dailyPrices
                  (unsafeFromVector $ V.fromList [Iex.Keep, Iex.Keep, Iex.Keep])
          capitals `shouldBe` V.fromList [100, 100, 100]
      describe "always buy" $
        it "capital fluctuates with stock price" $ do
          let dailyPrices =
                unsafeFromVector $
                mkDailyPrices
                  ( unsafeMkDate (2019, D.January, 1)
                  , unsafeMkDate (2019, D.January, 3))
                  [110, 111, 117]
          let capitals =
                the $
                Iex.calcCapitals
                  110
                  dailyPrices
                  (unsafeFromVector $ V.fromList [Iex.Buy, Iex.Buy, Iex.Buy])
          capitals `shouldBe` V.fromList [110, 111, 117]
      describe "mixture of buy, keep, sell" $
        it "capital fluctuates correctly" $ do
          let dailyPrices =
                unsafeFromVector $
                mkDailyPrices
                  ( unsafeMkDate (2019, D.January, 1)
                  , unsafeMkDate (2019, D.January, 6))
                  [110, 111, 117, 115, 112, 109]
          let capitals =
                the $
                Iex.calcCapitals
                  1000
                  dailyPrices
                  (unsafeFromVector $
                   V.fromList
                     [Iex.Buy, Iex.Sell, Iex.Keep, Iex.Buy, Iex.Keep, Iex.Sell])
          capitals `shouldBe`
            V.fromList
              [10 + 9 * 110, 1009, 1009, 89 + 8 * 115, 89 + 8 * 112, 961]
    describe "calcSmaStrategies" $
      it "preserves length" $
      property $
      forAll (unsafeFromVector <$> arbitrary) $ \dps ->
        V.length (the dps) `shouldBe`
        (V.length . the . Iex.calcSmaStrategies $ dps)
