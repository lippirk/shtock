{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Spec.IndicatorsSpec
  ( spec
  ) where

import qualified Data.Newtypes   as N
import           Data.Vec        (Vec, fromVec, sizing, unsafeFromVector,
                                  withVec)
import qualified Data.Vector     as V
import           Instances       ()
import           Math.Indicators (exponentialMovingAverage, simpleMovingAverage)
import           Test.Hspec
import           Test.QuickCheck
import           Util

spec :: Spec
spec = do
  describe "simpleMovingAverage" $ do
    it "3 [1, 2, 3, 4] == [1, 1.5, 2, 3]" $
      sizing (V.fromList [1, 2, 3, 4]) $ \xs ->
        fromVec (simpleMovingAverage (N.Nat 3) xs) `shouldBe`
        V.fromList [1, 1.5, 2, 3]
    it "leaves singleton vectors unchanged" $
      property $ \(n :: N.Nat) (x :: Double) ->
        sizing (V.singleton x) $ \xs ->
          the (simpleMovingAverage n xs) `shouldBe` V.singleton x
  describe "exponentialMovingAverage" $ do
    it "ema 1 == id" $
      forAll nonEmptyVector $
      withVec (\xs -> exponentialMovingAverage 1 xs `shouldBe` xs)
    it "ema 0.5 [1, 2, 3] == [1, 1.5, 2.25]" $
      sizing (V.fromList [1, 2, 3]) $ \xs ->
        the (exponentialMovingAverage 0.5 xs) `shouldBe`
        V.fromList [1, 1.5, 2.25]
