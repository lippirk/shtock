{-# LANGUAGE ScopedTypeVariables #-}

module Instances
  (
  ) where

import           Data.Date       (DateN, mkDateN)
import qualified Data.Newtypes   as N
import qualified Data.Vector     as V
import           Generic.Random
import qualified Iex.Strategies  as Iex
import qualified Iex.Types       as Iex
import           Snap            (MonadSnap, Snap)
import qualified Snap            as S
import           Test.QuickCheck
import           Time.Types      (Month)

--------------------------------------------------------------------------------
instance Arbitrary N.Nat where
  arbitrary = N.Nat <$> ((arbitrary :: Gen Int) `suchThat` (>= 1))
  -- guess at good instance for shrink?
  shrink (N.Nat 1) = []
  shrink (N.Nat x) = fmap N.Nat [1 .. (x - 1)]

instance Arbitrary DateN where
  arbitrary = do
    maybeDates :: [(N.Nat, Month, N.Nat)] <- infiniteList
    go maybeDates
    where
      go ((y, m, d):others) =
        case mkDateN y m d of
          Nothing -> go others
          Just d  -> return d

instance Arbitrary Iex.DailyPrice where
  arbitrary = Iex.DailyPrice <$> arbitrary <*> arbitrary

instance Arbitrary Iex.Strategy where
  arbitrary = genericArbitraryU

instance Arbitrary Month where
  arbitrary = do
    i <- choose (0, 11)
    return $ toEnum i

instance Arbitrary a => Arbitrary (Vector a) where
  arbitrary = V.fromList <$> arbitrary
  shrink = fmap V.fromList . shrink . V.toList
