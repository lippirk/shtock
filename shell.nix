with import ./.;

let

  tools = with haskellPackages; with pkgs; [

    # haskell
    cabal-install
    hlint
    hfmt
    ghcid
    hoogle

    # js
    nodejs

    # editor
    (my-haskell-nvim {
      moduleSearchPaths = "server/src:server/app:server/test";
      ignoreLinters = "['eslint']";
    })

  ];

in
  haskellPackages.shellFor {
    packages = p:  [ shtock ];
    withHoogle = true;
    buildInputs = tools;
  }


