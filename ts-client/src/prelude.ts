export const assertNever = (x: never): never => {
  throw new Error("Unexpected object: " + x);
};

export interface NamedHTMLElement extends HTMLElement {
  name: string;
}
