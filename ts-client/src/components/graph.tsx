import React, { Component } from "react";
import { assertNever } from "../prelude";
import {
  LineChart,
  XAxis,
  YAxis,
  CartesianGrid,
  ReferenceLine,
  Line,
  Tooltip
} from "recharts";

export interface GraphProps {
  data: Array<object>;
  xKey: string;
  yLine: YLine;
  otherYLines: Array<YLine>;
  yRefLines: Array<number>;
}

interface YLine {
  yKey: string;
  renderDot?: (dotProps: any) => JSX.Element;
}

export const Graph = (props: GraphProps) => {
  const referenceLines = props.yRefLines.map((y, i) => {
    return (
      <ReferenceLine
        key={`yRefLine-${i}`}
        y={y}
        stroke="red"
        strokeDasharray="3 3"
      />
    );
  });

  const mainLine = (
    <Line
      dataKey={props.yLine.yKey}
      stroke="#8884d8"
      dot={props.yLine.renderDot ? props.yLine.renderDot : true}
    />
  );

  const otherYLines = props.otherYLines.map((ol, i) => (
    <Line
      key={`otherYLine-${i}`}
      dataKey={ol.yKey}
      stroke="#8884d8"
      dot={ol.renderDot ? ol.renderDot : false}
    />
  ));

  // XAxis interval = 0 => all ticks
  return (
    <LineChart
      width={600}
      height={400}
      data={props.data}
      style={{ margin: 10 }}
    >
      <CartesianGrid stroke="#ccc" />
      <XAxis
        dataKey={props.xKey}
        tick={<XAxisTick />}
        interval={1}
        height={50}
        tickMargin={5}
      />
      {mainLine}
      {otherYLines}
      {referenceLines}
      <YAxis domain={["auto", "auto"]} tick={<YAxisTick />} />
      <Tooltip />
    </LineChart>
  );
};

const XAxisTick = (props: any) => {
  const { x, y, stroke, payload } = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text
        style={{ fontSize: 10 }}
        x={0}
        y={0}
        textAnchor="end"
        fill="#666"
        transform="rotate(-40)"
      >
        {payload.value}
      </text>
    </g>
  );
};

const YAxisTick = (props: any) => {
  const { x, y, stroke, payload } = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text style={{ fontSize: 10 }} x={0} y={0} textAnchor="end" fill="#666">
        {payload.value}
      </text>
    </g>
  );
};
