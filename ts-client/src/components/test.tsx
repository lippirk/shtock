import React, { Component } from "react";
import { Graph, GraphProps } from "./graph";
import { StrategyData } from "./form";

const mockStrategyData: StrategyData = {
  name: "sma",
  historicPerformance: [
    1000,
    950,
    925,
    1100,
    1222,
    1000,
    1050,
    1025,
    1001,
    1100
  ]
};

export const TestStrategyData = (props: {}) => {
  const data = mockStrategyData.historicPerformance.map((p, i) => ({
    x: i,
    y: p
  }));
  return (
    <Graph
      data={data}
      xKey={"x"}
      yLine={{ yKey: "y" }}
      otherYLines={[]}
      yRefLines={[]}
    />
  );
  return null;
};
