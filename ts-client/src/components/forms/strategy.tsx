import React, { Component } from "react";
import { Graph, GraphProps } from "../graph";
import * as P from "../../prelude";
import * as S from "../../core/server";

interface IStrategyDisplayState {
  graph: GraphProps | undefined;
}

export class StrategyDisplay extends Component<{}, IStrategyDisplayState> {
  constructor(props: {}) {
    super(props);
    this.updateDisplay = this.updateDisplay.bind(this);
    this.state = {
      graph: undefined
    };
  }

  updateDisplay = (strategyData: S.StrategyData) => {
    const yRefLines = [1000];
    const others: { key: string; data: number[] }[] = [];

    const data = strategyData.historicPerformance.map((y, i) => ({
      y: y,
      x: i
    }));

    const graph = {
      data: data,
      xKey: "x",
      yLine: { yKey: "y" },
      otherYLines: [],
      yRefLines: yRefLines
    };

    this.setState({ graph: graph });
  };

  render() {
    const graph1 =
      this.state.graph == null ? null : <Graph {...this.state.graph} />;
    return (
      <React.Fragment>
        <Strategy onResponse={this.updateDisplay} />
        {graph1}
      </React.Fragment>
    );
  }
}

export class Strategy extends Component<IFormResponseHandler, IForm> {
  constructor(props: IFormResponseHandler) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = { symbol: "aapl" };
  }

  handleChange = (e: React.ChangeEvent<P.NamedHTMLElement>) => {
    const t = e.target;
    const name = t.name;
    if (name == symbolInputName) {
      const symbolInput = t as HTMLInputElement;
      this.setState({ symbol: symbolInput.value });
    } else {
      throw new Error(`Unknown form change: ${name}`);
    }
  };

  handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    const { symbol } = this.state;

    const rq = `/stock/strategy/q?symbol=${symbol}`;

    const resp = await fetch(rq);

    if (resp.status !== 200) {
      throw new Error(`Chart request failed with code: ${resp.status}`);
    }

    // not sure what happens if this fails (cannot extract anything from docs...)
    // will cross that bridge if necessary
    const json: S.Response<S.StrategyData> = await resp.json();

    if (json.status != "ok" || json.result == null) {
      throw new Error("Could not fetch data");
    }

    this.props.onResponse(json.result);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          value={this.state.symbol}
          name={symbolInputName}
          onChange={this.handleChange}
        />
        <input type="submit" value="Go" />
      </form>
    );
  }
}

const symbolInputName = "symbol-input";

interface IForm {
  symbol: string;
}

interface IFormResponseHandler {
  onResponse: (resp: S.StrategyData) => void;
}
