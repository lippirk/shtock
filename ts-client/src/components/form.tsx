import React, { Component } from "react";
import { Graph, GraphProps } from "./graph";
import { assertNever, NamedHTMLElement } from "../prelude";
import { Dot } from "recharts";
import * as P from "../prelude";
import * as S from "../core/server";

/* -------------------------------------------------------------------------- */

interface ChartDisplayState {
  graph1: GraphProps | undefined;
  graph2: GraphProps | undefined;
}

export class ChartDisplay extends Component<{}, ChartDisplayState> {
  constructor(props: {}) {
    super(props);
    this.updateDisplay = this.updateDisplay.bind(this);
    this.state = {
      graph1: undefined,
      graph2: undefined
    };
  }

  // Recharts requires all data to be in one array, e.g.
  // [ { date: "a", close: 1, sma: 1   }
  // , { date: "b", close: 2, sma: 1.5 }
  // , ...
  // ]
  mergeData = (
    dps: [S.DailyPrice],
    others: { key: string; data: number[] }[]
  ): S.DailyPrice[] => {
    const newData: S.DailyPrice[] = [];
    for (let i = 0; i < dps.length; i++) {
      const newObj: any = { ...dps[i] };
      for (let j = 0; j < others.length; j++) {
        const other = others[j];
        newObj[other.key] = other.data[i];
      }
      newData.push(newObj);
    }

    return newData;
  };

  updateDisplayWithChartData = (chart: S.ChartData) => {
    const yRefLines = chart.maximum == null ? [] : [chart.maximum];
    const others: { key: string; data: number[] }[] = [];

    let closePriceRenderDot = undefined;

    let smaLine = undefined;
    if (chart.sma != null) {
      others.push({ key: "sma", data: chart.sma });
      smaLine = { yKey: "sma" };
      closePriceRenderDot = (dotProps: any) => {
        const { payload } = dotProps;
        const stroke =
          payload["close"] < payload["sma"] ? "#ff0000" : "#008000";

        return <Dot {...dotProps} stroke={stroke} fill={stroke} />;
      };
    }

    let macdLine = undefined;
    if (chart.macd != null) {
      others.push({ key: "macd", data: chart.macd });
      macdLine = { yKey: "macd" };
    }

    const data = this.mergeData(chart.dailyPrices, others);

    const graph1 = {
      data: data,
      xKey: "date",
      yLine: { yKey: "close", renderDot: closePriceRenderDot },
      otherYLines: smaLine == null ? [] : [smaLine],
      yRefLines: yRefLines
    };

    const graph2 =
      macdLine == null
        ? undefined
        : {
            data: data,
            xKey: "date",
            yLine: { yKey: "macd" },
            otherYLines: [],
            yRefLines: [0]
          };

    this.setState({ graph1: graph1, graph2: graph2 });
  };

  updateDisplay = (r: S.Charts) => {
    const chart = Object.values(r)[0];
    this.updateDisplayWithChartData(chart);
  };

  render() {
    const graph1 =
      this.state.graph1 == null ? null : <Graph {...this.state.graph1} />;
    const graph2 =
      this.state.graph2 == null ? null : <Graph {...this.state.graph2} />;
    return (
      <React.Fragment>
        <ChartForm onResponse={this.updateDisplay} />
        {graph1}
        {graph2}
      </React.Fragment>
    );
  }
}

class ChartForm extends Component<IFormResponseHandler, IForm> {
  constructor(props: IFormResponseHandler) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);

    const twoWeeksAgo = new Date();
    twoWeeksAgo.setDate(twoWeeksAgo.getDate() - 14);

    const oneDayAgo = new Date();
    oneDayAgo.setDate(oneDayAgo.getDate() - 1);

    this.state = {
      indicator: "maximum",
      symbol: "msft",
      startDate: twoWeeksAgo.toISOString().substr(0, 10),
      endDate: oneDayAgo.toISOString().substr(0, 10)
    };
  }

  handleChange(e: React.ChangeEvent<NamedHTMLElement>) {
    const t = e.target;
    const name = t.name;
    if (name == symbolInputName) {
      const symbolInput = t as HTMLInputElement;
      this.setState({ symbol: symbolInput.value });
    } else if (name === indicatorSelectName) {
      const indicatorSelect = t as HTMLSelectElement;
      this.setState({ indicator: indicatorSelect.value });
    } else if (name === startDateInputName) {
      const startDateInput = t as HTMLInputElement;
      this.setState({ startDate: startDateInput.value });
    } else if (name === endDateInputName) {
      const endDateInput = t as HTMLInputElement;
      this.setState({ endDate: endDateInput.value });
    } else {
      throw new Error(`Unknown form change: ${name}`);
    }
  }

  async handleSubmit(e: React.FormEvent) {
    e.preventDefault();

    const { symbol, indicator, startDate, endDate } = this.state;

    const rq = `/stock/chart/q?symbol=${symbol}&indicator=${indicator}&start-date=${startDate}&end-date=${endDate}`;

    const resp = await fetch(rq);

    if (resp.status !== 200) {
      throw new Error(`Chart request failed with code: ${resp.status}`);
    }

    // not sure what happens if this fails (cannot extract anything from docs...)
    // will cross that bridge if necessary
    const json: S.Response<S.Charts> = await resp.json();

    if (json.status != "ok" || json.result == null) {
      throw new Error("Could not fetch data");
    }

    this.props.onResponse(json.result);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          type="text"
          value={this.state.symbol}
          name={symbolInputName}
          onChange={this.handleChange}
        />
        <input
          type="date"
          className="date-input"
          name={startDateInputName}
          value={this.state.startDate}
          onChange={this.handleChange}
        />
        <input
          type="date"
          className="date-input"
          name={endDateInputName}
          value={this.state.endDate}
          onChange={this.handleChange}
        />
        <select
          value={this.state.indicator}
          onChange={this.handleChange}
          name={indicatorSelectName}
        >
          <option value="maximum">Maximum</option>
          <option value="simple-moving-average">Moving average</option>
        </select>
        <input type="submit" value="Go" />
      </form>
    );
  }
}
/* -------------------------------------------------------------------------- */

interface IForm {
  symbol: string;
  indicator: string;
  startDate: string;
  endDate: string;
}

interface IFormResponseHandler {
  onResponse: (resp: S.Charts) => void;
}

const indicatorSelectName = "indicator-select";
const symbolInputName = "symbol-input";
const startDateInputName = "start-date-input";
const endDateInputName = "end-date-input";

/* -------------------------------------------------------------------------- */
