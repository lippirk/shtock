export interface Response<T> {
  status: string;
  code: number;
  error?: any;
  result?: T & { type: string };
}

export interface Charts {
  [ticker: string]: ChartData;
}

export interface ChartData {
  dailyPrices: [DailyPrice];
  maximum?: number;
  sma?: number[];
  macd?: number[];
}

export interface DailyPrice {
  date: string;
  close: number;
}

export interface StrategyData {
  name: string;
  historicPerformance: number[];
}

// interface Response {
//   status: string;
//   code: number;
//   error?: any;
//   result?: Charts & { type: "chart" };
// }
