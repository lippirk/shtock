import React from "react";
import { ChartDisplay } from "./components/form";
import { TestStrategyData } from "./components/test";
import * as Form from "./components/forms/strategy";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from "react-router-dom";

const App = () => (
  <React.Fragment>
    <header>
      <span>shtock v0.1.0</span>
      <hr />
    </header>
    <main>{appRouter}</main>
  </React.Fragment>
);

const appRouter = (
  <Router>
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/chart/">Chart</Link>
          </li>
          <li>
            <Link to="/strategy/">Strategy</Link>
          </li>
        </ul>
      </nav>
      <Route exact path="/" render={() => <Redirect to="/chart" />} />
      <Route default path="/chart/" exact component={ChartDisplay} />
      <Route path="/strategy/" component={Form.StrategyDisplay} />
    </div>
  </Router>
);

export default App;
